# 01-first-function-with-sat

## Install the Suborbital CLI

> https://github.com/suborbital/subo

```bash
brew tap suborbital/subo
brew install subo
```

## Install Sat

> https://github.com/suborbital/sat

```bash
git clone https://github.com/suborbital/sat.git
cd sat
make sat
sudo cp .bin/sat /usr/local/bin/
```

## Create a Runnable with Subo

```bash
subo create runnable hello
```
> This command will generate a Rust project, have a look 👀 to the source code: `./hello/src/lib.rs`

```bash
subo build ./hello
```
> This 2nd command will build the wasm file: `./hello/hello.wasm`

## Serve the WebAssembly function (Runnable)

```bash
SAT_HTTP_PORT=8080 sat hello/hello.wasm
```

Logs:
```bash
{"log_message":"(W) configured to use HTTP with no TLS","timestamp":"2022-01-26T06:43:36.841357919Z","level":2}
{"log_message":"(I) starting hello ...","timestamp":"2022-01-26T06:43:36.841890307Z","level":3}
{"log_message":"(I) serving on :8080","timestamp":"2022-01-26T06:43:36.841961977Z","level":3}
{"log_message":"(I) [discovery-local] starting discovery, advertising endpoint 8080 /meta/message","timestamp":"2022-01-26T06:43:36.841951464Z","level":3}
```

## Call the function

```bash
curl localhost:8080 -d 'Bob Morane'
# response: Hello Bob Morane
```

## Blog posts about Sat

- https://gitlab.com/k33g_org/k33g_org.gitlab.io/-/issues/98
- https://gitlab.com/k33g_org/k33g_org.gitlab.io/-/issues/99
- https://gitlab.com/k33g_org/k33g_org.gitlab.io/-/issues/100


